<?php
//use Illuminate\Support\Facades\Input;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// rest react native routes

		Route::get('/api/cats', [
			'uses' => 'RNProductController@getCats',
			'as' => 'product.getcatsrn'
		]);
		Route::get('/api/follows/{userid}', [
			'uses' => 'RNProductController@getFollows',
			'as' => 'product.getfollowsrn'
		]);
		Route::post('/api/checkout/{userid}', [
			'uses' => 'RNProductController@postCheckout',
			'as' => 'product.postcheckoutrn'
		]);
		Route::get('/api/increment/{id}', [
			'uses' => 'RNProductController@IncrementByOne',
			'as' => 'product.incrementByOnern'
		]);
		Route::get('/api/decrement/{id}', [
			'uses' => 'RNProductController@ReduceByOne',
			'as' => 'product.decrementByOnern'
		]);
		Route::get('/api/remove/{id}', [
			'uses' => 'RNProductController@RemoveItem',
			'as' => 'product.removern'
		]);
		Route::get('/api/add-to-cart/{id}',[
			'uses'=>'RNProductController@getAddToCart',
			'as'=>'product.addToCartrn'
		]);
		Route::get('/api/shopping-cart', [
			'uses' => 'RNProductController@getCart',
			'as' => 'product.shoppingCartrn'
		]);
		Route::get('/api/notification/{userid}',[
			'uses'=>'RNUserController@getNotification',
			'as'=>'user.getnotificationrn'
		]);
		Route::get('/api/csb',[
			'uses'=>'RNProductController@getCsb',
			'as'=>'user.csbrn'
		]);
		Route::get('/api/profile/{userid}',[
			'uses'=>'RNUserController@getProfile',
			'as'=>'user.getprofilern'
		]);
		Route::get('/api/notify/{id}/{userid}',[
			'uses'=>'RNUserController@notify',
			'as'=>'user.notifyrn'
		]);
		Route::post('/api/changeemail/{userid}',[
			'uses'=>'RNUserController@postChangeEmail',
			'as'=>'user.changeemailrn'
		]);
		Route::post('/api/changepass/{userid}',[
			'uses'=>'RNUserController@postChangePass',
			'as'=>'user.changepassrn'
		]);
		Route::post('/api/signup',[
			'uses'=>'RNUserController@postSignup',
			'as'=>'user.signuprn'
		]);
		Route::post('/api/signin',[
			'uses'=>'RNUserController@postSignin',
			'as'=>'user.signinrn'
		]);

		Route::get('/api/allindex/{userid}',[
			'uses'=>'RNProductController@getIndex',
			'as'=>'product.getindexrn'
		]);

		Route::get('/api/catindex/{subcatid}/{userid}',[
			'uses'=>'RNProductController@getCatIndex',
			'as'=>'product.getcatindexrn'
		]);
		Route::get('/api/brandindex/{brandid}/{userid}',[
			'uses'=>'RNProductController@getBrandIndex',
			'as'=>'product.getbrandindexrn'
		]);
		Route::post('/api/searchindex/{userid}',[
			'uses'=>'RNProductController@getSearchIndex',
			'as'=>'product.getsearchindexrn'
		]);
		Route::get('/api/artan/{subcatid}/{markaid}/{userid}',[
			'uses'=>'RNProductController@getArtan',
			'as'=>'product.getartanrn'
		]);
		Route::get('/api/azalan/{subcatid}/{markaid}/{userid}',[
			'uses'=>'RNProductController@getAzalan',
			'as'=>'product.getazalanrn'
		]);
		Route::get('/api/productdetails/{productid}/{userid}',[
			'uses'=>'RNProductController@getProductDetails',
			'as'=>'product.getproductdetailsrn'
		]);
		Route::get('/api/follow/{productid}/{userid}',[
			'uses'=>'RNProductController@follow',
			'as'=>'product.followrn'
		]);
		Route::post('/api/addcomment/{userid}/{productid}',[
			'uses'=>'RNProductController@postAddComment',
			'as'=>'product.addcommentrn'
		]);

		Route::get('/api/increment/{id}', [
			'uses' => 'RNProductController@IncrementByOne',
			'as' => 'product.incrementByOnern'
		]);

		Route::get('/api/products', 'RNProductController@getProducts');

// -end- rest react native routes

Route::post('/postcomment',[
    'uses' => 'ProductController@postAddComment',
    'as' => 'postcomment'
]);

Route::get('/', [
		'uses' => 'ProductController@getIndex',
		'as' => 'product.index'
]);

Route::get('/urunler/{id}', [
    'uses' => 'ProductController@getCatIndex',
    'as' => 'product.catindex'
]);

Route::get('/urunara', [
    'uses' => 'ProductController@getSearchIndex',
    'as' => 'product.searchindex'
]);

Route::get('/storage/{filename}', [
    'uses' => 'ProductController@getProductImage',
    'as' => 'product.image'
]);

Route::get('/urundetaylari/{id}',[
	'uses'=>'ProductController@getProductDetails',
	'as'=>'product.details'
]);

Route::get ('/artan/{subcatid}/{markaid}',[
  'uses'=>'ProductController@getArtan',
  'as'=> 'sort.artan'
]);

Route::get('/azalan/{subcatid}/{markaid}',[
  'uses'=>'ProductController@getAzalan',
  'as'=>'sort.azalan'
]);

Route::get('/marka/{id}',[
    'uses'=>'ProductController@getBrandIndex',
    'as'=>'product.brands'
]);
Route::get('/add-to-cart/{id}',[
	'uses'=>'ProductController@getAddToCart',
	'as'=>'product.addToCart'
]);

Route::post('/add-to-cart-qty',[
    'uses'=>'ProductController@postAddToCartQty',
    'as'=>'product.addToCartQty'

]);

Route::get('/reduce/{id}', [
    'uses' => 'ProductController@getReduceByOne',
    'as' => 'product.reduceByOne'
]);

Route::get('/increment/{id}', [
    'uses' => 'ProductController@getIncrementByOne',
    'as' => 'product.incrementByOne'
]);

Route::get('/remove/{id}', [
    'uses' => 'ProductController@getRemoveItem',
    'as' => 'product.remove'
]);

Route::get('/json-add-to-cart/{id}','ProductController@addToCartQty');
Route::get('/json-remove/{id}','ProductController@RemoveItem');
Route::get('/json-increment/{id}','ProductController@IncrementByOne');
Route::get('/json-reduce/{id}','ProductController@ReduceByOne');
Route::get('/json-set/{id}/{q}','ProductController@SetItemQty');
Route::get('/json-zamanlama/{id}','ProductController@Zamanlama');

Route::get('/json-bitis/{id}','ProductController@getBitis');

Route::get('/shopping-cart', [
    'uses' => 'ProductController@getCart',
    'as' => 'product.shoppingCart'
]);

Route::get('/checkout',[
	'uses'=>'ProductController@getCheckout',
	'as'=>'checkout',
	'middleware'=>'noguest'
]);


Route::post('/checkout', [
    'uses' => 'ProductController@postCheckout',
    'as' => 'checkout',
    'middleware' => 'noguest'
]);

Route::get('/json-follow/{id}','ProductController@follow');
Route::get('/json-unfollow/{id}','ProductController@unfollow');

Route::post('/follow', [
    'uses' => 'ProductController@postFollow',
    'as' => 'follow',
    'middleware' => 'noguest'
]);
Route::post('/unfollow', [
    'uses' => 'ProductController@postUnfollow',
    'as' => 'unfollow',
    'middleware' => 'noguest'
]);

Route::get('/bildirim',[
	'uses'=>'UserController@getNotification',
	'as'=>'user.notification',
	'middleware'=>'noguest'
]);

Route::group(['middleware'=>'adminonly'],function(){

		Route::get('/urunekle',[
				'uses'=>'AdminController@getAddProduct',
				'as'=>'admin.addProduct'
		]);

    Route::post('/urunekle', [
        'uses' => 'AdminController@postAddProduct',
        'as' => 'admin.addProduct'
    ]);


		Route::get('/urunsil/{id}', [
				'uses' => 'AdminController@getRemoveProduct',
				'as' => 'admin.removeProduct'
		]);

    Route::post('/urunsil', [
        'uses' => 'AdminController@postRemoveProduct',
        'as' => 'admin.removeProduct'
    ]);

		Route::get('/anakategorisil/{id}', [
				'uses' => 'AdminController@getRemoveCat',
				'as' => 'admin.removeCat'
		]);

		Route::post('/anakategorisil', [
				'uses' => 'AdminController@postRemoveCat',
				'as' => 'admin.removeCat'
		]);

		Route::get('/altkategorisil/{id}', [
				'uses' => 'AdminController@getRemoveSubcat',
				'as' => 'admin.removeSubcat'
		]);

		Route::post('/altkategorisil', [
				'uses' => 'AdminController@postRemoveSubcat',
				'as' => 'admin.removeSubcat'
		]);


		Route::get('/urunduzenle/{id}', [
				'uses' => 'AdminController@getEditProduct',
				'as' => 'admin.editProduct'
		]);

		Route::post('/urunduzenle', [
				'uses' => 'AdminController@postEditProduct',
				'as' => 'admin.editProduct'
		]);

		Route::get('/anakategoriduzenle/{id}', [
				'uses' => 'AdminController@getEditCat',
				'as' => 'admin.editCat'
		]);

		Route::post('/anakategoriduzenle', [
				'uses' => 'AdminController@postEditCat',
				'as' => 'admin.editCat'
		]);

		Route::get('/altkategoriduzenle/{id}', [
				'uses' => 'AdminController@getEditSubcat',
				'as' => 'admin.editSubcat'
		]);

		Route::post('/altkategoriduzenle', [
				'uses' => 'AdminController@postEditSubcat',
				'as' => 'admin.editSubcat'
		]);


		Route::get('/anakategoriekle',[
        'uses'=>'AdminController@getAddCat',
        'as'=>'admin.addCat'
    ]);


    Route::post('/anakategoriekle', [
        'uses' => 'AdminController@postAddCat',
        'as' => 'admin.addCat'
    ]);

		Route::get('/altkategoriekle',[
				'uses'=>'AdminController@getAddSubCat',
				'as'=>'admin.addSubCat'
		]);


		Route::post('/altkategoriekle', [
				'uses' => 'AdminController@postAddSubCat',
				'as' => 'admin.addSubCat'
		]);
    Route::get('/markaekle',[
        'uses'=>'AdminController@getAddBrand',
        'as'=>'admin.addBrand'
    ]);
    Route::get('/markaduzenle/{id}',[
        'uses'=>'AdminController@getEditBrand',
        'as'=>'admin.editBrand'
    ]);
    Route::get('/markasil/{id}',[
        'uses'=>'AdminController@getRemoveBrand',
        'as'=>'admin.removeBrand'
    ]);

    Route::post('/markaekle',[
        'uses'=>'AdminController@postAddBrand',
        'as'=>'admin.addBrand'
    ]);
    Route::post('/markaduzenle',[
        'uses'=>'AdminController@postEditBrand',
        'as'=>'admin.editBrand'
    ]);
    Route::post('/markasil',[
        'uses'=>'AdminController@postRemoveBrand',
        'as'=>'admin.removeBrand'
    ]);

		Route::get('/adminpanel',[
				'uses'=>'AdminController@getAdminPanel',
				'as'=>'admin.panel'
		]);


});


Route::group(['prefix'=>'user'],function(){

Route::group(['middleware'=>'guest'],function(){


	Route::get('/signup',[
		'uses'=>'UserController@getSignup',
		'as'=>'user.signup'
	]);
	Route::post('/signup',[
		'uses'=>'UserController@postSignup',
		'as'=>'user.signup'

	]);

	Route::get('/signin',[
		'uses'=>'UserController@getSignin',
		'as'=>'user.signin'
	]);
	Route::post('/signin',[
		'uses'=>'UserController@postSignin',
		'as'=>'user.signin'
	]);



});

Route::group(['middleware'=>'noguest'] , function(){

	Route::get('/profile',[
		'uses'=>'UserController@getProfile',
		'as'=>'user.profile'
	]);
	Route::get('/logout',[
		'uses'=>'UserController@getLogout',
		'as'=>'user.logout'
	]);
  Route::post('/changeemail',[
    'uses'=>'UserController@postChangeEmail',
    'as'=>'user.changeemail'
  ]);
  Route::post('/changepass',[
    'uses'=>'UserController@postChangePass',
    'as'=>'user.changepass'
  ]);
});

});


Route::get('/json-subcats/{id}','AdminController@subcats');
Route::get('/json-notify/{id}','UserController@notify');
