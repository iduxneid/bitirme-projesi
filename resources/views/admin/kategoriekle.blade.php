@extends('layouts.master')

@section('title')
    Kategori Ekleme Ekranı
@endsection


@section('content')
<div class="row">
  <div class="col-md-4 col-md-offset-4">
<h1>Ana Kategori Ekle</h1>
@if(count($errors)>0)
<div class="alert alert-danger">
  @foreach($errors->all() as $error)
<p>{{$error}}</p>
@endforeach
</div>
@endif
<form action="{{ route('admin.addCat') }}" method="post">
    <div class="form-group">
        <label for="name">Kategori Adı</label>
        <input type="text" id="name" name="name" class="form-control">
    </div>
    <button type="submit" class="btn btn-primary">Ana Kategori Ekle</button>
    {{csrf_field()}}
</form>

  </div>
</div>

@endsection
