@extends('layouts.master')

@section('title')
    Alt Kategori Silme Ekranı
@endsection


@section('content')
<div class="row">
  <div class="col-md-4 col-md-offset-4">
<h2>Alt Kategori Silme İşlemi</h2>
@if(count($errors)>0)
<div class="alert alert-danger">
  @foreach($errors->all() as $error)
<p>{{$error}}</p>
@endforeach
</div>
@endif
<form action="{{ route('admin.removeSubcat') }}" method="post">
    <div class="form-group">
        <label for="name">Bu alt kategoriyi gerçekten silmek istiyor musunuz?</label>
        <select name="id" >

<option value="{{$subcat->id}}>">{{$subcat->name}}</option>

        </select>
    </div>
    <button type="submit" class="btn btn-primary">Alt Kategoriyi Sil</button>
    {{csrf_field()}}
</form>

  </div>
</div>

@endsection
