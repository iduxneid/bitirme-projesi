@extends('layouts.master')

@section('title')
    Kategori Düzenleme Ekranı
@endsection


@section('content')
<div class="row">
  <div class="col-md-4 col-md-offset-4">
<h1>Ana Kategori Duzenle</h1>
@if(count($errors)>0)
<div class="alert alert-danger">
  @foreach($errors->all() as $error)
<p>{{$error}}</p>
@endforeach
</div>
@endif
<form action="{{ route('admin.editCat') }}" method="post">
    <div class="form-group">
        <label for="name">Kategori Adı</label>
        <input type="hidden" id="id" name="id" class="form-control" value="{{$cat->id}}">
        <input type="text" id="name" name="name" class="form-control" value="{{$cat->name}}">
    </div>
    <button type="submit" class="btn btn-primary">Ana Kategori Düzenle</button>
    {{csrf_field()}}
</form>

  </div>
</div>

@endsection
