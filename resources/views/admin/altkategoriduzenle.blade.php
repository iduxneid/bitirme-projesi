@extends('layouts.master')


@section('title')
    Alt Kategori Düzenleme Ekranı
@endsection


@section('content')
<div class="row">
  <div class="col-md-4 col-md-offset-4">
<h1>Alt Kategori Duzenle</h1>
@if(count($errors)>0)
<div class="alert alert-danger">
  @foreach($errors->all() as $error)
<p>{{$error}}</p>
@endforeach
</div>
@endif
<form action="{{ route('admin.editSubcat') }}" method="post">
  <div class="form-group">
      <label for="name">Ana Kategorisi</label>
      <select id="cat_id" name="cat_id">
@foreach($cats as $cat)
<option value="{{$cat->id}}"
@if($cat->id==$subcat->cat->id)

selected

@endif
  >{{$cat->name}}</option>
@endforeach
      </select>
  </div>
    <div class="form-group">
        <label for="name">Alt Kategori Adı</label>
        <input type="hidden" id="id" name="id" class="form-control" value="{{$subcat->id}}">
        <input type="text" id="name" name="name" class="form-control" value="{{$subcat->name}}">
    </div>
    <button type="submit" class="btn btn-primary">Alt Kategori Duzenle</button>
    {{csrf_field()}}
</form>

  </div>
</div>

@endsection
