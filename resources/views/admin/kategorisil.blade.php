@extends('layouts.master')


@section('title')
    Kategori Silme Ekranı
@endsection

@section('content')
<div class="row">
  <div class="col-md-4 col-md-offset-4">
<h2>Ana Kategori Silme İşlemi</h2>
@if(count($errors)>0)
<div class="alert alert-danger">
  @foreach($errors->all() as $error)
<p>{{$error}}</p>
@endforeach
</div>
@endif
<form action="{{ route('admin.removeCat') }}" method="post">
    <div class="form-group">
        <label for="name">Bu ana kategoriyi gerçekten silmek istiyor musunuz?</label>
        <select name="id" >

<option value="{{$cat->id}}>">{{$cat->name}}</option>

        </select>
    </div>
    <button type="submit" class="btn btn-primary">Ana Kategoriyi Sil</button>
    {{csrf_field()}}
</form>

  </div>
</div>

@endsection
