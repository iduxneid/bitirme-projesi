@extends('layouts.master')

@section('title')
Hemen İndirim - Ürünlerimiz
@endsection


@section('content')
	@include('partials.sidebarleft')
	@include('partials.urunlistesi')
@endsection

@section('scripts')
@include('partials.listescripti')
<script src="{{URL::to('js/jquery.countdown.min.js')}}"></script>
@endsection
