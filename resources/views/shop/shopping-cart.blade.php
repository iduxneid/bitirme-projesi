@extends('layouts.master')

@section('title')
    Sepet
@endsection

@section('content')
@if(Session::has('cart'))
	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Liste</a></li>
				  <li class="active">Sepet</li>
				</ol>
			</div>
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Ürün</td>
							<td class="description"></td>
							<td class="price">Fiyat</td>
							<td class="quantity">Adet</td>
							<td class="total">Toplam</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
					@foreach($products as $product)
						<tr id="tr{{ $product['item']['id'] }}">
							<td class="cart_product">
								<a href="">
                  <div style="background-image: url('{{ route('product.image', ['filename' => $product['item']['imageName']]) }}'); display: block; background-size: 70px auto; background-repeat: no-repeat; background-position: center; width: auto; height: 100px;">
                </div>

							</td>
							<td class="cart_description" style="padding-left: 50px; width: 40%;">
								<h4><a href="">{{ $product['item']['title'] }}</a></h4>
							</td>
							<td class="cart_price">
								<p>{{ number_format($product['price'] / $product['qty'], 2, ',', '.')}} TL</p>
							</td>
							<td class="cart_quantity">
								<div class="cart_quantity_button">
									<?php /* <a id="up{{ $product['item']['id'] }}" class="cart_quantity_up" href="{{ route('product.incrementByOne', ['id' => $product['item']['id']]) }}"> + </a>
								*/ ?>  <a id="up{{ $product['item']['id'] }}" class="cart_quantity_up" href="{{ route('product.incrementByOne', ['id' => $product['item']['id']]) }}"> + </a>
									<input id="set{{ $product['item']['id'] }}" class="cart_quantity_input" type="text" name="quantity" value="{{ $product['qty'] }}" autocomplete="off" size="2">
									<a id="down{{ $product['item']['id'] }}" class="cart_quantity_down" href="{{ route('product.reduceByOne', ['id' => $product['item']['id']]) }}"> - </a>
									<?php /* <a id="down{{ $product['item']['id'] }}" class="cart_quantity_down" href="{{ route('product.reduceByOne', ['id' => $product['item']['id']]) }}"> - </a>
								*/ ?>
								</div>
							</td>
							<td class="cart_total">
								<p id="total{{ $product['item']['id'] }}" class="cart_total_price">{{ number_format($product['price'], 2, ',', '.') }} TL</p>
							</td>
							<td>
								<a id="delete{{ $product['item']['id'] }}" class="cart_quantity_delete" href="{{ route('product.remove', ['id' => $product['item']['id']]) }}"><i class="fa fa-times fa-2x"></i></a>
							<?php /* 	<a id="delete{{ $product['item']['id'] }}" class="cart_quantity_delete" href="{{ route('product.remove', ['id' => $product['item']['id']]) }}"><i class="fa fa-times"></i></a>
						 */ ?>	</td>
						</tr>
					@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</section> <!--/#cart_items-->

	<section id="do_action">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="total_area">
						<ul>
							<li>Toplam Fiyat: <span><p id="toplam-fiyat" class="toplam-fiyat">{{ number_format($totalPrice, 2, ',', '.') }} TL</p></span></li>
						</ul>
							<a class="btn btn-default check_out pull-right" href="{{ route('checkout') }}">Alışverişi Tamamla</a>
							<div style="clear:both;"></div>
					</div>
				</div>
			</div>
		</div>
	</section><!--/#do_action-->
@else
        <div class="row">
            <div class="col-sm-12">
                <h2>Sepette ürün yok!</h2>
            </div>
        </div>
@endif
@endsection

@section('scripts')
    <script type="text/javascript">
	$(".cart_quantity_input").on('input propertychange paste', function(e) {
		var qty = e.target.value;
        if(qty == 0){
            qty = 1;
            $(".cart_quantity_input").val(1).change();
        }
		var id = e.target.id;
		var item_id = id.slice(3);
        $.get('{{URL::to('json-set')}}/' + item_id + '/' + qty,function(data) {
          jsonData = JSON.stringify(data);
          var jsonObj = JSON.parse(jsonData);
          $('#total' + item_id).text(jsonObj.price + " TL");
          $('.toplam-fiyat').text(jsonObj.totalPrice + " TL");
          $('#set' + item_id).val(jsonObj.quantity).change();
          $('#sepet-sayi').html(jsonObj.totalQty);
        });
	});
	$(".cart_quantity_up").on("click", function(e) {
		var id = $(this).attr('id');
		var item_id = id.slice(2);
        $.get('{{URL::to('json-increment')}}/' + item_id,function(data) {
            jsonData = JSON.stringify(data);
            var jsonObj = JSON.parse(jsonData);
            $('#total' + item_id).text(jsonObj.price + " TL");
            $('.toplam-fiyat').text(jsonObj.totalPrice + " TL");
            $('#set' + item_id).val(jsonObj.quantity).change();
            $('#sepet-sayi').text(jsonObj.totalQty);
        });
        e.preventDefault();
	});
	$(".cart_quantity_down").on("click", function(e) {
		var id = $(this).attr('id');
		var item_id = id.slice(4);
		var tr_id = "tr" + item_id;
        $.get('{{URL::to('json-reduce')}}/' + item_id,function(data) {
            jsonData = JSON.stringify(data);
            var jsonObj = JSON.parse(jsonData);
            if(jsonObj.noitem === true){
                $('.toplam-fiyat').text(jsonObj.totalPrice + " TL");
                if(jsonObj.totalQty < 1){
                    $('#sepet-sayi').text('');
                    $('#masterrow').html('<div class="row"><div class="col-sm-12"><h2>Sepette ürün yok!</h2></div></div>');
                }
                else{
                	$('#sepet-sayi').text(jsonObj.totalQty);
                }
                $('#'+ tr_id).remove();
            }
            else if (jsonObj.noitem === false){
                $('#total' + item_id).text(jsonObj.price + " TL");
                $('.toplam-fiyat').text(jsonObj.totalPrice + " TL");
                $('#set' + item_id).val(jsonObj.quantity).change();
                $('#sepet-sayi').text(jsonObj.totalQty);
            }
        });
        e.preventDefault();
	});
	$(".cart_quantity_delete").on("click", function(e) {
		var id = $(this).attr('id');
		var item_id = id.slice(6);
		var tr_id = "tr" + item_id;
        $.get('{{URL::to('json-remove')}}/' + item_id,function(data) {
          jsonData = JSON.stringify(data);
          var jsonObj = JSON.parse(jsonData);
          if(jsonObj.totalQty < 1){
              $('#sepet-sayi').text('');
              $('#masterrow').html('<div class="row"><div class="col-sm-12"><h2>Sepette ürün yok!</h2></div></div>');
          }
          else{
          	$('#sepet-sayi').text(jsonObj.totalQty);
          }
          $('#'+ tr_id).remove();
          $('.toplam-fiyat').text(jsonObj.totalPrice + " TL");
        });
        e.preventDefault();
	});
    </script>
@endsection
