@extends('layouts.master')

@section('title')
Hemen İndirim - Dönem Sonu Bitirme Projesi
@endsection

@section('slider')
	@include('partials.slider')
@endsection

@section('content')
	@include('partials.sidebarleft')
	@include('partials.urunlistesi')
@endsection

@section('scripts')
@include('partials.listescripti')
<script src="{{URL::to('js/jquery.countdown.min.js')}}"></script>
@endsection
