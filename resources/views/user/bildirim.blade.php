@extends('layouts.master')

@section('title')
    Tüm Bildirimler
@endsection

@section('content')
<h2>Tüm Bildirimler</h2>
<hr>
<div class="row">
<div class="col-sm-8 col-sm-offset-2">
<ul class="list-group">
@foreach($b_urunler as $b_urun)
  <li class="list-group-item">
<img class="image-responsive" width="50" height="50" src="{{ route('product.image', ['filename' => $b_urun->imageName]) }}" alt="Card image cap">

Takip ettiğiniz <a href="{{ route('product.details', ['id' => $b_urun->id]) }}">{{$b_urun->title}}</a> adlı ürün yüksek derecede indirim aldı.

</li>
@endforeach
</ul>
</div>
</div>


@endsection
