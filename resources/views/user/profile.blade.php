@extends('layouts.master')


@section('title')
    Profil
@endsection

@section('content')


@if(Session::has('success'))
   <div class="row">
     <div class="col-sm-6 col-md-4 col-md-offset-4 col-sm-offset-3">
       <div id="charge-message" class="alert alert-success">
         {{ Session::get('success') }}
       </div>
     </div>
   </div>
@endif
@if(Session::has('hata'))
<div class="row">
  <div class="col-sm-6 col-md-4 col-md-offset-4 col-sm-offset-3">
    <div id="charge-message" class="alert alert-danger">
      {{ Session::get('hata') }}
    </div>
  </div>
</div>
@endif



<div class="category-tab shop-details-tab"><!--category-tab-->
  <div class="col-sm-12">
    <ul class="nav nav-tabs">
      <li class="active"><a href="#previous_orders" data-toggle="tab">Önceki Siparişlerim</a></li>
      <li><a href="#follows" data-toggle="tab">Takip Ettiklerim</a></li>
      <li><a href="#settings" data-toggle="tab">Ayarlar</a></li>
    </ul>
  </div>
  <div class="tab-content">



    <div class="tab-pane fade active in" id="previous_orders" >


      <div class="row ">
        <div class="col-sm-10 col-sm-offset-1">
      <h1>Kullanıcı Profili</h1>
      <hr>
      <h2>Sipariş Geçmişi</h2>
      <div id="accordion" role="tablist">
        @foreach($orders as $order)
        <div class="card">
          <div class="card-header" role="tab" id="headingOne">
            <h5 class="mb-0">
              <a data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                {{$order['created_at']}} tarihinde satın alma işlemi gerçekleşti.
              </a>
            </h5>
          </div>

          <div id="{{$order['created_at']}}" class="collapse show" role="tabpanel" aria-labelledby="{{$order['created_at']}}" data-parent="#accordion">
            <div class="card-body">

              <ul class="list-group">
                @foreach($order->cart->items as $item)
                <li class="list-group-item">
      <span class="badge">{{$item['price']}} <i class="fa fa-try" aria-hidden="true"></i></span>
      {{$item['item']['title']}} ürününden, {{$item['qty']}} adet alındı.
                </li>
                @endforeach

                <li class="list-group-item list-group-item-secondary">Toplam Fiyat: {{ number_format($order->cart->totalPrice, 2, ',', '.') }} <i class="fa fa-try" aria-hidden="true"></i></li>
              </ul>

            </div>
          </div>
        </div>
      @endforeach
      </div>
        </div>
      </div>


    </div>



    <div class="tab-pane fade" id="follows" >
      <div class="col-sm-10 col-sm-offset-1">
      <table class="table table-condensed">

        <tbody>
      @foreach($b_urunler as $b_urun)
      <tr>
        <td>

      <div style="background-image: url('{{ route('product.image', ['filename' => $b_urun->imageName]) }}'); display: block; background-size: 50px auto; background-repeat: no-repeat; background-position: center; width: 50px; height: 80px;">
    </div>
  </td>
<td>
       <a href="{{ route('product.details', ['id' => $b_urun->id]) }}" style="display:inline;">{{$b_urun->title}}</a> adlı ürünü takip ediyorsunuz.
</td>
<td>
      <form  action="{{ route('unfollow') }}" method="post" class="pull-right" id="follow-form">
        {{ csrf_field() }}
        <input type="hidden" id="id" name="product_id" class="form-control" value="{{$b_urun->id}}">
      <button type="submit" class="btn btn-danger">Takipten Çıkar</button>
      </form>
</td>

    </tr>
      @endforeach
    </tbody>
    </table>
    </div>
    </div>


    <div class="tab-pane fade" id="settings" >
      <div class="col-sm-4 col-sm-offset-4">
      <form action="{{route('user.changeemail')}}" method="post">
      <div class="form-group">
      <label for="email email_button">E-Mail</label>
      <input type="text" id="email" name="email" class="form-control" placeholder="{{Auth::user()->email}}">
     <button type="submit" class="btn btn-primary email_button">Email Adresini Değiştir</button>
      </div>
      {{csrf_field()}}
      </form>

      <form action="{{route('user.changepass')}}" method="post">
      <div class="form-group">
      <label for="password password_button">Şifre</label>
      <input type="password" id="password" name="password" class="form-control" placeholder="İstediğiniz yeni şifre...">
     <button type="submit" class="btn btn-primary email_button">Şifreni Değiştir</button>
      </div>
      {{csrf_field()}}
      </form>
</div>
    </div>



  </div>
</div><!--/category-tab-->







@endsection
