@php
use Carbon\Carbon;
setlocale(LC_TIME, 'Turkish');
@endphp
<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>@yield('title')</title>
    <link href="{{URL::to('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{URL::to('css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{URL::to('css/prettyPhoto.css')}}" rel="stylesheet">
    <link href="{{URL::to('css/price-range.css')}}" rel="stylesheet">
    <link href="{{URL::to('css/animate.css')}}" rel="stylesheet">
	<link href="{{URL::to('css/main.css')}}" rel="stylesheet">
  <link href="{{URL::to('css/notification.css')}}" rel="stylesheet">
  <link href="{{URL::to('css/richtext.min.css')}}" rel="stylesheet">
	<link href="{{URL::to('css/responsive.css')}}" rel="stylesheet">
	<link href="{{URL::to('css/jquery-ui.min.css')}}" rel="stylesheet">
	<link href="{{URL::to('css/myapp.css')}}" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
    <style type="text/css">

    </style>
    @yield('styles')
</head>
<body>
	@include('partials.header')
	@yield('slider')
	<section>
	<div class="container">
		<div id="masterrow" class="row">
			@yield('content')
		</div>
	</div>
	</section>
	@include('partials.footer')

	<script src="{{URL::to('js/jquery-3.2.1.min.js')}}"></script>
	<script src="{{URL::to('js/jquery-ui.min.js')}}"></script>
	<script src="{{URL::to('js/bootstrap.min.js')}}"></script>
	<script src="{{URL::to('js/jquery.scrollUp.min.js')}}"></script>
	<script src="{{URL::to('js/price-range.js')}}"></script>
    <script src="{{URL::to('js/jquery.prettyPhoto.js')}}"></script>
    <script src="{{URL::to('js/main.js')}}"></script>
    <script src="{{URL::to('js/notification.js')}}"></script>
    <script src="{{URL::to('js/jquery.richtext.js')}}"></script>
    <script src="{{URL::to('js/jquery.countdown.min.js')}}"></script>
    <script>
    $.get('{{URL::to('json-notify')}}/0',function(data) {
        //alert(data);
        $('#notify').empty();
        $('#notify').append('<div id="notificationsBody" class="notifications"></div>');
        var bildirimBoyut = 0;
        $.each(JSON.parse(data), function(idx) {
            bildirimBoyut += 1;
        });
		if(bildirimBoyut > 0){
			console.log(bildirimBoyut);
$('#notificationLink').html("<i class='fa fa-bell' style='color:red;' aria-hidden='true'></i> Bildirimler<span class='badge'>"+ (bildirimBoyut) +"</span>");
		}


      });
    $("#notificationLink").on("click", function(e) {
      console.log(e);

          $.get('{{URL::to('json-notify')}}/1',function(data) {
            //alert(data);
            $('#notify').empty();
            $('#notify').append('<div id="notificationsBody" class="notifications"></div>');
			console.log(data);
            $.each(JSON.parse(data), function(idx, notifies) {

                $('#notificationsBody').append("<div class='notifications' style='font-size: 0.7em; margin: 1em; border: 2px solid #ff5555; border-radius: 35px;'><img class='image-responsive' style='border:2px solid #ff5555; border-radius: 50%;' width='50' height='50' src='<?php echo url('/') ?>/storage/"+notifies.imageName+"' alt='Card image cap'><a href='<?php echo url('/') ?>/urundetaylari/"+notifies.id+"'>"+notifies.title+"</a> adlı ürün büyük derecede indirim aldı.</div>");




                          })

          });

    });

    </script>
    @yield('scripts')
</body>
</html>
