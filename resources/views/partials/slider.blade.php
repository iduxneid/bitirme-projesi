	<section id="slider"><!--slider-->
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div id="slider-carousel" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
							<li data-target="#slider-carousel" data-slide-to="1"></li>
							<li data-target="#slider-carousel" data-slide-to="2"></li>
							<li data-target="#slider-carousel" data-slide-to="3"></li>
						</ol>
@php
$sayac=0;
$randomProducts = \App\Product::orderBy(DB::raw('RAND()'))->take(4)->get();
@endphp

<div class="carousel-inner">
@foreach($randomProducts as $sliderProduct)
							@if($sayac==0)
							<div class="item active">
								@else
								<div class="item">
								@endif
								<div class="col-sm-6">
									<h1>{{$sliderProduct->title}}</h1>
									<p>{!!$sliderProduct->description!!}</p>
									<a href="{{ route('product.details', ['id' => $sliderProduct->id]) }}"><button type="button" class="btn btn-default get">Şimdi Satın Al</button></a>
								</div>
								<div class="col-sm-6">
									<div style="background-image: url('{{ route('product.image', ['filename' => $sliderProduct->imageName]) }}'); display: block; background-size: 300px auto; background-repeat: no-repeat; background-position: center; width: auto; height: 450px;">
									
								</div>
								</div>
							</div>
@php
							$sayac++;
@endphp
	@endforeach


						</div>

						<a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
							<i class="fa fa-angle-left"></i>
						</a>
						<a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
							<i class="fa fa-angle-right"></i>
						</a>
					</div>

				</div>
			</div>
		</div>
	</section><!--/slider-->
