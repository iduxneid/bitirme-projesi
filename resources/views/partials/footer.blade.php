	<footer id="footer"><!--Footer-->

		<div class="footer-widget">
			<div class="container">
				<div class="row">
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>Servis</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="#">Online Yardım</a></li>
								<li><a href="#">İletişim Kurun</a></li>
								<li><a href="#">Sipariş Durumu</a></li>
								<li><a href="#">Bölde Değişimi</a></li>
								<li><a href="#">SSS'lar</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>Ürün Mağazası</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="#">Kitaplar</a></li>
								<li><a href="#">Taşıtlar</a></li>
								<li><a href="#">Giyim</a></li>
								<li><a href="#">Elektronik</a></li>
								<li><a href="#">Ayakkabılar</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>Poliçe</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="#">Kullanım Şartnamesi</a></li>
								<li><a href="#">İçerik Kuralları</a></li>
								<li><a href="#">Geri İade Poliçesi</a></li>
								<li><a href="#">Faturalama Sistemi</a></li>
								<li><a href="#">Ticket Sistemi</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>Hakkımızda</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="#">Şirket Bilgisi</a></li>
								<li><a href="#">Kariyer</a></li>
								<li><a href="#">Mağaza Konumu</a></li>
								<li><a href="#">Bağlı Şirketler</a></li>
								<li><a href="#">İletişim</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="companyinfo">
							<br>
							<a href="{{route('product.index')}}"><span>Hemen</span> İndirim</a>
							<p>Anlık indirimler ve daha fazlası.</p>
						</div>
					</div>

				</div>
			</div>
		</div>

		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<p class="pull-left">© 2017 HEMEN-İNDİRİM. Tüm hakları saklıdır.</p>
				</div>
			</div>
		</div>

	</footer><!--/Footer-->
