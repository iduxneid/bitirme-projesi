<script>
var farukid = $(".faruk").attr('id');
$("#"+farukid).click();

$(".addtocart").on("click", function(e) {
	var id = $(this).attr('id');
	var item_id = id.slice(3);
	var item_title = $("#etc" + item_id).html();
	console.log(item_id);
    $.get('{{URL::to('json-add-to-cart')}}/' + item_id,function(data) {
        console.log("json test");
        jsonData = JSON.stringify(data);
        var jsonObj = JSON.parse(jsonData);
        $('#sepet-sayi').text(jsonObj.totalQty);
        if(jsonObj.islem==true){
			var testAnimation = true;
        }
        else{
            var testAnimation = false;
        }
        if (testAnimation === true){
				if(jsonObj.islem==true){
                	$("#etc" + item_id).html("<p style=\"color:green;\" class=\"animated pulse sepeteklendi\">Sepete eklendi!</p>");
                	setTestAnimation(false);
				}
				else{
					$("#etc" + item_id).html("<p style=\"color:red;\" class=\"animated pulse sepeteklendi\">Bu üründen daha fazla sepete ekleyemezsiniz!</p>");
        			setTestAnimation(false);
				}
        }
    });
    setTimeout(function(){
		$("#etc" + item_id).html(item_title);
		setTestAnimation(true);
	}, 1500);
    e.preventDefault();
});
function setTestAnimation(bool){
	testAnimation = bool;
}
</script>

<script>

$(".addtotakip").on("click", function(e) {
	var id = $(this).attr('id');
	var item_id = id.slice(3);
    $.get('{{URL::to('json-follow')}}/' + item_id,function(data) {
        jsonData = JSON.stringify(data);
        var jsonObj = JSON.parse(jsonData);
        if (jsonObj.cevap === true){
            console.log("Takip ediliyor");
        	$("#" + id).html("<i class='fa fa-minus-square' aria-hidden='true'></i> Takibi Bırak");}
        else{
        	console.log("Takip edilmiyor");
        	$("#" + id).html("<i class='fa fa-plus-square' aria-hidden='true'></i> Takip Et");}
    });
    e.preventDefault();
});
</script>

<script>
var myVar = setInterval(myTimer, 3000);
function myTimer() {
$('.addtocart').each(function(i, obj) {
		var id = $(this).attr("id");
		var item_id = id.slice(3);
		var priceBefore = $("#cp" + item_id).html();
		$.get('{{URL::to('json-zamanlama')}}/' + item_id,function(data) {
	         jsonData = JSON.stringify(data);
	         jsonObj = JSON.parse(jsonData);
	         if (priceBefore != (jsonObj.cp + " TL")){
    	         $("#cp" + item_id).html(jsonObj.cp + " TL");
    	         $("#cp" + item_id).css("color", "orange");
							 $("#cp" + item_id).addClass("animated pulse");
	         }
	    });
});
setTimeout(function(){
	$(".currentpriceholder").css("color", "#ff5555");
	$(".currentpriceholder").removeClass("animated pulse");
}, 1000);

}
</script>

<script>
$('.gerisayim').each(function(i, obj) {
	var cid = $(this).attr("id");
	var cid_id = cid.slice(4);
	$.get('{{URL::to('json-bitis')}}/' + cid_id,function(data) {
        jsonData = JSON.stringify(data);
        jsonObj = JSON.parse(jsonData);
    	$('#saat' + cid_id).countdown(jsonObj.end_date)
    	.on('update.countdown', function(event) {
    	  var format = '%H:%M:%S';
    	  if(event.offset.totalDays > 0) {
    	    format = '%-d gün ' + format;
    	  }
    	  if(event.offset.weeks > 0) {
    	    format = '%-w hafta ' + format;
    	  }
    	  $(this).html(event.strftime(format));
    	})
    	.on('finish.countdown', function(event) {
    	  $(this).html('Süresi doldu!')
    	    .parent().addClass('disabled');

    	});

   });
});

</script>
