<header id="header"><!--header-->

		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="logo pull-left">

								<div class="companyinfo">
									<a href="{{route('product.index')}}"><span>Hemen</span> İndirim</a>
								</div>



						</div>

					</div>
					<div class="col-sm-8">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
								@if(Auth::check())
								@if(Auth::user()->access==1)
								<li><a href="{{route('admin.panel')}}"><i class="fa fa-user"></i> Admin Paneli</a></li>
								@endif
								<li><a href="{{route('user.profile')}}"><i class="fa fa-user"></i> Hesap</a></li>




								<li id="notification_li">

								<a href="#" id="notificationLink"><i class="fa fa-bell" aria-hidden="true"></i> Bildirimler</a>
								<!--<span id="notification_count"></span>-->


									<div id="notificationContainer">
									<div id="notificationTitle">Bildirimler</div>
									<div id="notify">

								</div>
									<a href="{{route('user.notification')}}"><div id="notificationFooter">Tüm Bildirimleri Gör</div></a>
									</div>

								</li>
















								@endif
								<li><a href="{{route('checkout')}}"><i class="fa fa-crosshairs"></i> Alışverişi Tamamla</a></li>
								<li><a href="{{route('product.shoppingCart')}}"><i class="fa fa-shopping-cart"></i> Sepet <span id="sepet-sayi" class="badge badge-secondary">{{Session::has('cart') ? Session::get('cart')->totalQty : ''}}</span></a></li>
								@if(Auth::check())
								<li><a href="{{route('user.logout')}}"><i class="fa fa-sign-out" aria-hidden="true"></i> Çıkış Yap</a></li>
								@else
								<li><a href="{{route('user.signin')}}"><i class="fa fa-lock"></i> Giriş</a></li>
								<li><a href="{{route('user.signup')}}"><i class="fa fa-user-plus" aria-hidden="true"></i> Kayıt Ol</a></li>
								@endif
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->

		<div class="header-bottom"><!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-12">




						<div class="pull-right">
							<form action="{{ route('product.searchindex') }}" method="get">
															<div class="productsrc">
																	<input type="text" class="form-control input-sm" name="searchString" maxlength="64" placeholder="Ara..." />
																	<button type="submit" class="btn btn-fefault cart btn-sm srcbtn">Ara</button>
															</div>
							</form>
					</div>


					</div>
				</div>
			</div>
		</div><!--/header-bottom-->
	</header><!--/header-->
