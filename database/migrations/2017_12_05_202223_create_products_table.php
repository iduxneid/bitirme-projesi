<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->dateTime('begin_date');
            $table->dateTime('end_date');
            $table->string('imageName')->default('default.jpg');
            $table->string('image1')->default('default.jpg');
            $table->string('image2')->default('default.jpg');
            $table->string('image3')->default('default.jpg');
            $table->string('title');
            $table->text('description');
            $table->text('rich_description');
            $table->integer('qty');
            $table->decimal('price', 13, 4);
            $table->decimal('current_price', 13, 4);
            $table->decimal('minprice', 13, 4);
            $table->integer('durum')->default(1);
            $table->integer('cat_id')->unsigned();
            $table->integer('subcat_id')->unsigned();
            $table->integer('brand_id')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
