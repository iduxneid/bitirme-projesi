<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	protected $fillable = ['end_date', 'title', 'description', 'qty', 'price', 'minprice'];

	public function cat()
	{
	return $this->belongsTo('App\Cat');
	}
	
	public function brand()
	{
	    return $this->belongsTo('App\Brand');
	}

	public function subcat()
	{
	return $this->belongsTo('App\Subcat');
	}
	public function scopeSearch($query, $s)
	{
	    return $query->where('title', 'like', '%' .$s. '%')->orWhere('description', 'like', '%' .$s. '%');
	}

    public function follows()
    	{
    	return $this->belongsTo('App\Follow');
    }
    public function comments()
    {
        return $this->hasMany('App\Comment');
    }
}
