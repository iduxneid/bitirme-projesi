<?php
namespace App\Http\Controllers;

use App\Brand;
use App\Cart;
use App\Comment;
use App\Product;
use App\Follow;
use App\Order;
use App\Subcat;
use App\Cat;
use Auth;
use Faker\Provider\tr_TR\DateTime;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests;
use Session;
use Carbon\Carbon;
use DB;

class ProductController extends Controller
{

    public function getIndex()
    {
        $brands = Brand::all();
        $categories = Cat::with('subcats')->get();
        $products = Product::where('durum', 1)->orderBy('id','desc')->paginate(6);
        $products = ProductController::priceUpdate($products);
        if (! is_null(Auth::user())) {
            $user_id = Auth::user()->id;
            $follows = Follow::where('user_id', $user_id);
            return view('shop.index', [
                'products' => $products,
                'categories' => $categories,
                'follows' => $follows,
                'brands' => $brands
            ]);
        } else {
            return view('shop.index', [
                'products' => $products,
                'categories' => $categories,
                'brands' => $brands
            ]);
        }
    }

    public function getCatIndex($id)
    {
        $categories = Cat::with('subcats')->get();
        $brands = Brand::all();
        $activeCatId = Subcat::find($id)->cat_id;
        $products = Product::where('durum', 1)->where('subcat_id', $id)->orderBy('id','desc')->paginate(6);
        $products = ProductController::priceUpdate($products);

        if (! is_null(Auth::user())) {
            $user_id = Auth::user()->id;
            if (isset($user_id)) {
                $follows = Follow::where('user_id', $user_id);
                return view('shop.products', [
                    'products' => $products,
                    'categories' => $categories,
                    'activeCatId' => $activeCatId,
                    'follows' => $follows,
                    'brands' => $brands,
                    'subcatid' => $id
                ]);
            }
        } else {
            return view('shop.products', [
                'products' => $products,
                'categories' => $categories,
                'activeCatId' => $activeCatId,
                'brands' => $brands,
                'subcatid' => $id
            ]);
        }
    }

    public function getBrandIndex($id)
    {
        $categories = Cat::with('subcats')->get();
        $brands = Brand::all();
        $products = Product::where('durum', 1)->where('brand_id', $id)->orderBy('id','desc')->paginate(6);
        $products = ProductController::priceUpdate($products);

        if (! is_null(Auth::user())) {
            $user_id = Auth::user()->id;
            if (isset($user_id)) {
                $follows = Follow::where('user_id', $user_id);
                return view('shop.products', [
                    'products' => $products,
                    'categories' => $categories,
                    'follows' => $follows,
                    'brands' => $brands,
                    'markaid' => $id
                ]);
            }
        } else {
            return view('shop.products', [
                'products' => $products,
                'categories' => $categories,
                'brands' => $brands,
                'markaid' => $id
            ]);
        }
    }

    public function getSearchIndex(Request $request)
    {
        $brands = Brand::all();
        $categories = Cat::with('subcats')->get();
        $s = $request->input('searchString');
        $products = Product::search($s)->where('durum', 1)->paginate(6);
        $products = ProductController::priceUpdate($products);
        if (! is_null(Auth::user())) {
            $user_id = Auth::user()->id;
            if (isset($user_id)) {
                $follows = Follow::where('user_id', $user_id);
                return view('shop.products', [
                    'products' => $products,
                    'categories' => $categories,
                    'follows' => $follows,
                    'brands' => $brands
                ]);
            }
        } else {
            return view('shop.products', [
                'products' => $products,
                'categories' => $categories,
                'brands' => $brands
            ]);
        }
    }

    public function getArtan($subcatid, $markaid){
      if (! is_null(Auth::user())) {
          $user_id = Auth::user()->id;
          if (isset($user_id)) {
              $follows = Follow::where('user_id', $user_id);
          }
      }
      $brands = Brand::all();
      $categories = Cat::with('subcats')->get();
      if($subcatid != 0){
        $products = Product::where('durum', 1)->where('subcat_id', $subcatid)->orderBy('current_price','asc')->paginate(6);
      }
      else if($markaid != 0){
        $products = Product::where('durum', 1)->where('brand_id', $markaid)->orderBy('current_price','asc')->paginate(6);
      }
      else{
        $products = Product::where('durum', 1)->orderBy('current_price','asc')->paginate(6);
      }

      $products = ProductController::priceUpdate($products);
      if (! is_null(Auth::user())) {
          $user_id = Auth::user()->id;
          $follows = Follow::where('user_id', $user_id);
          return view('shop.products', [
              'products' => $products,
              'categories' => $categories,
              'follows' => $follows,
              'brands' => $brands,
              'markaid' => $markaid,
              'subcatid' => $subcatid
          ]);
      } else {
          return view('shop.products', [
              'products' => $products,
              'categories' => $categories,
              'brands' => $brands,
              'markaid' => $markaid,
              'subcatid' => $subcatid
          ]);
      }
    }

    public function getAzalan($subcatid, $markaid){
      if (! is_null(Auth::user())) {
          $user_id = Auth::user()->id;
          if (isset($user_id)) {
              $follows = Follow::where('user_id', $user_id);
          }
      }
      $brands = Brand::all();
      $categories = Cat::with('subcats')->get();
      if($subcatid != 0){
        $products = Product::where('durum', 1)->where('subcat_id', $subcatid)->orderBy('current_price','desc')->paginate(6);
      }
      else if($markaid != 0){
        $products = Product::where('durum', 1)->where('brand_id', $markaid)->orderBy('current_price','desc')->paginate(6);
      }
      else{
        $products = Product::where('durum', 1)->orderBy('current_price','desc')->paginate(6);
      }

      $products = ProductController::priceUpdate($products);
      if (! is_null(Auth::user())) {
          $user_id = Auth::user()->id;
          $follows = Follow::where('user_id', $user_id);
          return view('shop.products', [
              'products' => $products,
              'categories' => $categories,
              'follows' => $follows,
              'brands' => $brands,
              'markaid' => $markaid,
              'subcatid' => $subcatid
          ]);
      } else {
          return view('shop.products', [
              'products' => $products,
              'categories' => $categories,
              'brands' => $brands,
              'markaid' => $markaid,
              'subcatid' => $subcatid
          ]);
      }
    }




    public function getAddToCart(Request $request, $id)
    {
        $product = Product::find($id);
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->add($product, $product->id);
        $request->session()->put('cart', $cart);
		return redirect()->route('product.index');
    }

    public function postAddToCartQty(Request $request)
    {
        $product = Product::find($request['p_id']);
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        if($request['qty'] > $product->qty){
          $hata="İstediğiniz adette ürün stokta bulunmamakta.";
        return redirect()->route('product.index')->with('hata',$hata);
        }else{
        $cart->incrementByQty($product, $product->id, $request['qty']);
        $request->session()->put('cart', $cart);
        return redirect()->route('product.index');
        }
    }
    public function addToCartQty(Request $request, $id)
    {
        $product = Product::find($id);
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $qty = 1;

        if(isset($cart->items[$id]) && ($cart->items[$id]['qty']>= $product->qty)){
          $request->session()->put('cart', $cart);
          $islem=false;
          $totalQty = $cart->totalQty;
          return response()->json([
              'totalQty' => $totalQty,
              'islem'=>$islem
          ]);
        }

        else{
          $cart->incrementByQty($product, $id, $qty);
          $request->session()->put('cart', $cart);
          $totalQty = $cart->totalQty;
          $islem=true;
          return response()->json([
              'totalQty' => $totalQty,
              'islem'=>$islem
          ]);
        }


    }

    public function getReduceByOne($id) {
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->reduceByOne($id);
        if (count($cart->items) > 0) {
            Session::put('cart', $cart);
        } else {
            Session::forget('cart');
        }
        return redirect()->route('product.shoppingCart');
    }

    public function getIncrementByOne($id) {
      $myproduct=Product::find($id);
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        if($cart->items[$id]['qty'] >= $myproduct->qty){
          $hata="İstediğiniz kadar ürün stokta yok.";
          return redirect()->route('product.index')->with('hata',$hata);
        }
          else{
        $cart->incrementByOne($id);
        if (count($cart->items) > 0) {
            Session::put('cart', $cart);
        } else {
            Session::forget('cart');
        }
        return redirect()->route('product.shoppingCart');
      }
    }
    public function getRemoveItem($id) {
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->removeItem($id);
        if (count($cart->items) > 0) {
            Session::put('cart', $cart);
        } else {
            Session::forget('cart');
        }
        return redirect()->route('product.shoppingCart');
    }

    public function ReduceByOne($id)
    {
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->reduceByOne($id);
        if (count($cart->items) > 0) {
            Session::put('cart', $cart);
        } else {
            Session::forget('cart');
        }
        if (isset($cart->items[$id])) {
            $price = $cart->items[$id]['price'];
            $totalPrice = $cart->totalPrice;
            $qty = $cart->items[$id]['qty'];
            $totalQty = $cart->totalQty;
            $noitem = false;
            $price = number_format($price, 2, ',', '.');
            $totalPrice = number_format($totalPrice, 2, ',', '.');
            return response()->json([
                'totalQty' => $totalQty,
                'noitem' => $noitem,
                'price' => $price,
                'totalPrice' => $totalPrice,
                'quantity' => $qty
            ]);
        } else {
            $totalPrice = $cart->totalPrice;
            $totalQty = $cart->totalQty;
            $noitem = true;
            $totalPrice = number_format($totalPrice, 2, ',', '.');
            return response()->json([
                'totalQty' => $totalQty,
                'noitem' => $noitem,
                'totalPrice' => $totalPrice
            ]);
        }
    }

    public function IncrementByOne(Request $request, $id)
    {
        $myproduct=Product::find($id);
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        if($cart->items[$id]['qty'] >= $myproduct->qty){
          $hata="İstediğiniz kadar ürün stokta yok.";
          $price = number_format($price, 2, ',', '.');
          $totalPrice = number_format($totalPrice, 2, ',', '.');
          return response()->json([
              'totalQty' => $totalQty,
              'price' => $price,
              'totalPrice' => $totalPrice,
              'quantity' => $qty
          ]);
        }
          else{
        $cart->incrementByOne($id);
        if (count($cart->items) > 0) {
            Session::put('cart', $cart);
        } else {
            Session::forget('cart');
        }
        $price = $cart->items[$id]['price'];
        $totalPrice = $cart->totalPrice;
        $qty = $cart->items[$id]['qty'];
        $totalQty = $cart->totalQty;
        $price = number_format($price, 2, ',', '.');
        $totalPrice = number_format($totalPrice, 2, ',', '.');
        return response()->json([
            'totalQty' => $totalQty,
            'price' => $price,
            'totalPrice' => $totalPrice,
            'quantity' => $qty
        ]);
      }
    }

    public function RemoveItem(Request $request, $id)
    {
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->removeItem($id);
        if (count($cart->items) > 0) {
            Session::put('cart', $cart);
        } else {
            Session::forget('cart');
        }
        $totalPrice = $cart->totalPrice;
        $totalQty = $cart->totalQty;
        $totalPrice = number_format($totalPrice, 2, ',', '.');
        return response()->json([
            'totalQty' => $totalQty,
            'totalPrice' => $totalPrice
        ]);
    }

    public function SetItemQty(Request $request, $id, $q)
    {
        $myproduct=Product::find($id);
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        if($q >= $myproduct->qty){

          $cart->setQty($id, $myproduct->qty);
          if (count($cart->items) > 0) {
              Session::put('cart', $cart);
          } else {
              Session::forget('cart');
          }
          $price = $cart->items[$id]['price'];
          $totalPrice = $cart->totalPrice;
          $totalQty = $cart->totalQty;
          $quantity=$myproduct->qty;
          $price = number_format($price, 2, ',', '.');
          $totalPrice = number_format($totalPrice, 2, ',', '.');
          return response()->json([
              'totalQty' => $totalQty,
              'price' => $price,
              'quantity'=>$quantity,
              'totalPrice' => $totalPrice
          ]);
        }else{
        $cart->setQty($id, $q);
        if (count($cart->items) > 0) {
            Session::put('cart', $cart);
        } else {
            Session::forget('cart');
        }
        $price = $cart->items[$id]['price'];
        $totalPrice = $cart->totalPrice;
        $totalQty = $cart->totalQty;
        $price = number_format($price, 2, ',', '.');
        $totalPrice = number_format($totalPrice, 2, ',', '.');
        return response()->json([
            'totalQty' => $totalQty,
            'price' => $price,
            'quantity'=>$cart->items[$id]['qty'],
            'totalPrice' => $totalPrice
        ]);
      }
    }

    public function getCart()
    {
        if (! Session::has('cart')) {
            return view('shop.shopping-cart');
        }
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        return view('shop.shopping-cart', [
            'products' => $cart->items,
            'totalPrice' => $cart->totalPrice
        ]);
    }

    public function getCheckout()
    {
        if (! Session::has('cart')) {
            return view('shop.shopping-cart');
        }
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        $total = $cart->totalPrice;
        return view('shop.checkout', [
            'total' => $total
        ]);
    }

    public function postCheckout(Request $request)
    {
        if (! Session::has('cart')) {
            return redirect()->route('shop.shoppingCart');
        }
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);

        try {
            $order = new Order();
            $order->cart = serialize($cart);
            foreach ($cart->items as $cartproduct) {
                $product = Product::find($cartproduct['item']['id']);
                $product->qty -= $cartproduct['qty'];
                if ($product->durum == 0) {
                    $errorstr = $product->title . " adlı ürün stokta kalmadı.";
                    return redirect()->route('checkout')->with('errorstr', $errorstr);
                } else {
                    if ($product->qty < 0) {
                        $errorstr = $product->title . " adlı ürün istediğiniz adette bulunmamakta.";
                        return redirect()->route('checkout')->with('errorstr', $errorstr);
                    } else if ($product->qty == 0) {
                        $product->durum = 0;
                        $product->save();
                    } else {
                        $product->save();
                    }
                }
            }
            $order->address = $request->input('address');
            $order->name = $request->input('name');

            Auth::user()->orders()->save($order);
        } catch (\Exception $e) {
            return redirect()->route('checkout')->with('error', $e->getMessage());
        }
        Session::forget('cart');
        return redirect()->route('product.index')->with('success', 'Ürün(ler) başarıyla satın alındı!');
    }

    public function getProductImage($filename)
    {
        $file = Storage::disk('public')->get($filename);
        return new Response($file, 200);
    }

    public function getProductDetails($id)
    {
        $brands = Brand::all();
        if (! is_null(Auth::user())) {
            $user_id = Auth::user()->id;
            $follows = Follow::all();
            if ($follows->where('product_id', $id)
                ->where('user_id', $user_id)
                ->first()) {
                $following = true;
            } else {
                $following = false;
            }
        }

        $categories = Cat::all();
        $product = Product::find($id);
        $comments = Comment::where('product_id', $id)->paginate(6);
        $product = ProductController::priceUpdateOne($product);
        if (! is_null(Auth::user())) {
        return view('shop.details', [
            'product' => $product,
            'categories' => $categories,
            'following' => $following,
            'brands' => $brands,
            'comments' => $comments
        ]);}
        else {
            return view('shop.details', [
                'product' => $product,
                'categories' => $categories,
                'brands' => $brands,
                'comments' => $comments
            ]);}
    }

    public function postFollow(Request $request)
    {
        $user_id = Auth::user()->id;
        $follows = Follow::all();
        if ($follows->where('product_id', $request['product_id'])
            ->where('user_id', $user_id)
            ->first()) {
            return redirect()->route('product.index')->with('success', 'Ürünü zaten takip ediyorsunuz!');
        } else {
            $follow = new Follow();
            $follow->user_id = Auth::user()->id;
            $follow->product_id = $request['product_id'];
            $follow->save();
            return redirect()->route('product.index')->with('success', 'Ürün takibe alındı!');
        }
    }
    public function follow(Request $request, $id)
    {
        $user_id = Auth::user()->id;
        $follows = Follow::where('product_id', $id)
        ->where('user_id', $user_id)
        ->first();
        if (isset($follows->user_id)) {
                $unfollow = Follow::where([
                    [
                        'user_id',
                        Auth::user()->id
                    ],
                    [
                        'product_id',
                        $id
                    ]
                ])->delete();
                $cevap = false;
                return response()->json(['cevap' => $cevap]);
            }
            else if (!isset($follows->user_id)){
                $follow = new Follow();
                $follow->user_id = Auth::user()->id;
                $follow->product_id = $id;
                $follow->save();
                $cevap = true;
                return response()->json(['cevap' => $cevap]);
            }
    }
    public function postUnfollow(Request $request)
    {
        $user_id = Auth::user()->id;
        $follows = Follow::all();
        if ($follows->where('product_id', $request{'product_id'})
            ->where('user_id', $user_id)
            ->first()) {
            $unfollow = Follow::where([
                [
                    'user_id',
                    Auth::user()->id
                ],
                [
                    'product_id',
                    $request['product_id']
                ]
            ])->delete();
            return redirect()->back()->with('success', 'Ürünü başarıyla takipten çıkarıldı!');
        } else {

            return redirect()->route('product.index')->with('success', 'Ürünü zaten takip etmiyorsunuz.');
        }
    }


    public function getBitis($id){
        $product = Product::find($id);
        $end_date = $product->end_date;
        return response()->json(['end_date' => $end_date]);
    }




    public function postAddComment(Request $request)
    {
        if(isset(Auth::user()->id)){
            $user_id = Auth::user()->id;
            $this->validate($request, [
                'ad' => 'required',
                'description' => 'required',
                'email' => 'email|required'
            ]);
            $comment = new Comment();
            $comment->user_id = $user_id;
            $comment->product_id = $request['prd_id'];
            $comment->name = $request['ad'];
            $comment->email = $request['email'];
            $comment->description = $request['description'];
            $comment->save();
            return redirect()->back();
        }
        else {
            return redirect()->back()->with('hata', 'Yorum yazabilmek için üye olmalısınız.');
        }
    }




    public function Zamanlama(Request $request, $id)
    {
            $product = Product::find($id);
            $product = ProductController::priceUpdateOne($product);
            $cp = number_format($product->current_price, 2, ',', '.');
            return response()->json(['cp' => $cp]);
    }


    public static function priceUpdate($products){
        foreach ($products as $product) {
            $beginDate = new Carbon($product->begin_date);
            $endDate = new Carbon($product->end_date);

            $diffInSeconds = $beginDate->diffInSeconds($endDate, false);
            if ($diffInSeconds > 0) {
                $secondPrice = ($product->price - $product->minprice) / $diffInSeconds;
                $now = Carbon::now();
                $product->current_price = $product->price - (($beginDate->diffInSeconds($now)) * $secondPrice);
            }
            if ($product->current_price < $product->minprice) {
                $product->current_price = $product->minprice;
            }
            $product->save();
        }
        return $products;
    }
    public static function priceUpdateOne($product){
        $beginDate = new Carbon($product->begin_date);
        $endDate = new Carbon($product->end_date);

        $diffInSeconds = $beginDate->diffInSeconds($endDate, false);
        if ($diffInSeconds > 0) {
            $secondPrice = ($product->price - $product->minprice) / $diffInSeconds;
            $now = Carbon::now();
            $product->current_price = $product->price - (($beginDate->diffInSeconds($now)) * $secondPrice);
        }
        if ($product->current_price < $product->minprice) {
            $product->current_price = $product->minprice;
        }
        $product->save();
        return $product;
    }
}
