<?php

namespace App\Http\Controllers;

use App\Brand;
use App\User;
use App\Cart;
use App\Comment;
use App\Product;
use App\Follow;
use App\Order;
use App\Subcat;
use App\Cat;
use Auth;
use Faker\Provider\tr_TR\DateTime;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests;
use Session;
use Carbon\Carbon;
use DB;

class RNProductController extends Controller
{

    public function getProductDetails($productid, $userid)
    {
        $brands = Brand::all();
        if ($userid != 0) {
            $user_id = $userid;
            $follows = Follow::all();
            if ($follows->where('product_id', $productid)
                ->where('user_id', $user_id)
                ->first()) {
                $following = true;
            } else {
                $following = false;
            }
        }

        $categories = Cat::all();
        $product = Product::find($productid);
        $comments = Comment::where('product_id', $productid)->get();
        $product = ProductController::priceUpdateOne($product);
        $cat = Cat::Find($product->cat_id);
        $subcat = Subcat::Find($product->subcat_id);
        $brand = Brand::Find($product->brand_id);
        if ($userid != 0) {
        return response()->json([
            'product' => $product,
            'categories' => $categories,
            'following' => $following,
            'brands' => $brands,
            'comments' => $comments,
            'cat' => $cat,
            'subcat' => $subcat,
            'brand' => $brand
        ]);}
        else {
            return response()->json([
                'product' => $product,
                'categories' => $categories,
                'brands' => $brands,
                'comments' => $comments,
                'cat' => $cat,
                'subcat' => $subcat,
                'brand' => $brand
            ]);}
    }

    public function getIndex($userid)
    {
        $brands = Brand::all();
        $categories = Cat::with('subcats')->get();
        $products = Product::where('durum', 1)->orderBy('id','desc')->get();
        $products = ProductController::priceUpdate($products);
        if ($userid != 0) {
            $user_id = $userid;
            if (isset($user_id)) {
                $follows = Follow::where('user_id', $user_id);
                return response()->json([
                    'products' => $products,
                    'categories' => $categories,
                    'follows' => $follows,
                    'brands' => $brands
                ]);
            }
        } else {
            return response()->json([
                'products' => $products,
                'categories' => $categories,
                'brands' => $brands
            ]);
        }
    }

    public function getCatIndex($subcatid, $userid)
    {
        $categories = Cat::with('subcats')->get();
        $brands = Brand::all();
        $activeCatId = Subcat::find($subcatid)->cat_id;
        $products = Product::where('durum', 1)->where('subcat_id', $subcatid)->orderBy('id','desc')->get();
        $products = ProductController::priceUpdate($products);

        if ($userid != 0) {
            $user_id = $userid;
            if (isset($user_id)) {
                $follows = Follow::where('user_id', $user_id);
                return response()->json([
                    'products' => $products,
                    'categories' => $categories,
                    'activeCatId' => $activeCatId,
                    'follows' => $follows,
                    'brands' => $brands,
                    'subcatid' => $subcatid
                ]);
            }
        } else {
            return response()->json([
                'products' => $products,
                'categories' => $categories,
                'activeCatId' => $activeCatId,
                'brands' => $brands,
                'subcatid' => $subcatid
            ]);
        }
    }

    public function getBrandIndex($brandid, $userid)
    {
        $categories = Cat::with('subcats')->get();
        $brands = Brand::all();
        $products = Product::where('durum', 1)->where('brand_id', $brandid)->orderBy('id','desc')->get();
        $products = ProductController::priceUpdate($products);

        if ($userid != 0) {
            $user_id = $userid;
            if (isset($user_id)) {
                $follows = Follow::where('user_id', $user_id);
                return response()->json([
                    'products' => $products,
                    'categories' => $categories,
                    'follows' => $follows,
                    'brands' => $brands,
                    'markaid' => $brandid
                ]);
            }
        } else {
            return response()->json([
                'products' => $products,
                'categories' => $categories,
                'brands' => $brands,
                'markaid' => $brandid
            ]);
        }
    }

    public function getSearchIndex(Request $request, $userid)
    {
        $brands = Brand::all();
        $categories = Cat::with('subcats')->get();
        $s = $request->input('searchString');
        $products = Product::search($s)->where('durum', 1)->get();
        $products = ProductController::priceUpdate($products);
        if ($userid != 0) {
            $user_id = $userid;
            if (isset($user_id)) {
                $follows = Follow::where('user_id', $user_id);
                return response()->json([
                    'products' => $products,
                    'categories' => $categories,
                    'follows' => $follows,
                    'brands' => $brands
                ]);
            }
        } else {
            return response()->json([
                'products' => $products,
                'categories' => $categories,
                'brands' => $brands
            ]);
        }
    }

    public function getArtan($subcatid, $markaid, $userid){
      if ($userid != 0) {
          $user_id = $userid;
          if (isset($user_id)) {
              $follows = Follow::where('user_id', $user_id);
          }
      }
      $brands = Brand::all();
      $categories = Cat::with('subcats')->get();
      if($subcatid != 0){
        $products = Product::where('durum', 1)->where('subcat_id', $subcatid)->orderBy('current_price','asc')->get();
      }
      else if($markaid != 0){
        $products = Product::where('durum', 1)->where('brand_id', $markaid)->orderBy('current_price','asc')->get();
      }
      else{
        $products = Product::where('durum', 1)->orderBy('current_price','asc')->get();
      }

      $products = ProductController::priceUpdate($products);
      if ($userid != 0) {
          $user_id = $userid;
          $follows = Follow::where('user_id', $user_id);
          return response()->json([
              'products' => $products,
              'categories' => $categories,
              'follows' => $follows,
              'brands' => $brands,
              'markaid' => $markaid,
              'subcatid' => $subcatid
          ]);
      } else {
          return response()->json([
              'products' => $products,
              'categories' => $categories,
              'brands' => $brands,
              'markaid' => $markaid,
              'subcatid' => $subcatid
          ]);
      }
    }

    public function getAzalan($subcatid, $markaid, $userid){
      if ($userid != 0) {
          $user_id = $userid;
          if (isset($user_id)) {
              $follows = Follow::where('user_id', $user_id);
          }
      }
      $brands = Brand::all();
      $categories = Cat::with('subcats')->get();
      if($subcatid != 0){
        $products = Product::where('durum', 1)->where('subcat_id', $subcatid)->orderBy('current_price','desc')->get();
      }
      else if($markaid != 0){
        $products = Product::where('durum', 1)->where('brand_id', $markaid)->orderBy('current_price','desc')->get();
      }
      else{
        $products = Product::where('durum', 1)->orderBy('current_price','desc')->get();
      }

      $products = ProductController::priceUpdate($products);
      if ($userid != 0) {
          $user_id = $userid;
          $follows = Follow::where('user_id', $user_id);
          return response()->json([
              'products' => $products,
              'categories' => $categories,
              'follows' => $follows,
              'brands' => $brands,
              'markaid' => $markaid,
              'subcatid' => $subcatid
          ]);
      } else {
          return response()->json([
              'products' => $products,
              'categories' => $categories,
              'brands' => $brands,
              'markaid' => $markaid,
              'subcatid' => $subcatid
          ]);
      }
    }

    public function follow($productid, $userid)
    {
        $user_id = $userid;
        $follows = Follow::where('product_id', $productid)
        ->where('user_id', $user_id)
        ->first();
        if (isset($follows->user_id)) {
                $unfollow = Follow::where([
                    [
                        'user_id',
                        $userid
                    ],
                    [
                        'product_id',
                        $productid
                    ]
                ])->delete();
                $cevap = false;
                return response()->json(['cevap' => $cevap]);
            }
            else if (!isset($follows->user_id)){
                $follow = new Follow();
                $follow->user_id = $userid;
                $follow->product_id = $productid;
                $follow->save();
                $cevap = true;
                return response()->json(['cevap' => $cevap]);
            }
    }

    public function getFollows($userid){
        $user_id = $userid;
        $products = Product::all();
        $follows = Follow::where('user_id', $user_id)->get();
        $i_urunler = array();
        $b_urunler = array();
        foreach ($products as $product) {
            $i_urunler[] = $product;
        }

        foreach ($follows as $follow) {
            foreach ($i_urunler as $i_urun) {
                if ($follow->product_id == $i_urun->id) {
                    $b_urunler[] = $i_urun;
                }
            }
        }

        $products = $b_urunler;
        return response()->json([
            'products' => $products
        ]);
    }
    public function getCats(){
        $categories = Cat::with('subcats')->get();
        return response()->json([ 'categories' => $categories ]);
    }
    public function postAddComment(Request $request, $userid, $productid)
    {
        if($userid != 0){
            $user_id = $userid;
            $this->validate($request, [
                'ad' => 'required',
                'description' => 'required',
                'email' => 'email|required'
            ]);
            $comment = new Comment();
            $comment->user_id = $user_id;
            $comment->product_id = $productid;
            $comment->name = $request['ad'];
            $comment->email = $request['email'];
            $comment->description = $request['description'];
            $comment->save();
            return response()->json([
                'success' => true
            ]);
        }
        else {
            return response()->json([
                'success' => false
            ]);
        }
    }

    public function getProducts(){
        $products = Product::all();
        $products = ProductController::priceUpdate($products);
        return response()->json($products);
    }
    public function getCsb(){
        $cats = Cat::all();
        $subcats = Subcat::all();
        $brands = Brand::all();
        return response()->json([
            'cats' => $cats,
            'subcats' => $subcats,
            'brands' => $brands
        ]);
    }
    public function getAddToCart(Request $request, $id)
    {
        $product = Product::find($id);
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->add($product, $product->id);
        $request->session()->put('cart', $cart);
        return response()->json(['cart' => $cart]);
    }

    public function IncrementByOne(Request $request, $id)
    {
        $myproduct=Product::find($id);
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        if($cart->items[$id]['qty'] >= $myproduct->qty){
          return response()->json([
              'cart' => $cart
          ]);
        }
        else{
            $cart->incrementByOne($id);
            if (count($cart->items) > 0) {
                Session::put('cart', $cart);
            } else {
                Session::forget('cart');
            }
            return response()->json([
                'cart' => $cart
            ]);
        }
    }

    public function getCart()
    {
        if (! Session::has('cart')) {
            return response()->json([
				'nocart' => true
			]);
        }
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        return response()->json(['cart' => $cart]);
    }

    public function RemoveItem(Request $request, $id)
    {
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->removeItem($id);
        if (count($cart->items) > 0) {
            Session::put('cart', $cart);
        } else {
            Session::forget('cart');
        }
        if (count($cart->items) > 0) {
            return response()->json([
                'cart' => $cart
            ]);
        } else {
            return response()->json([
                'cart' => null
            ]);
        }
    }

    public function ReduceByOne($id)
    {
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->reduceByOne($id);
        if (count($cart->items) > 0) {
            Session::put('cart', $cart);
        } else {
            Session::forget('cart');
        }
        if (isset($cart->items[$id])) {
            return response()->json([
                'cart' => $cart
            ]);
        } else {
            return response()->json([
                'cart' => $cart
            ]);
        }
    }

    public function postCheckout(Request $request, $userid)
    {
        if (! Session::has('cart')) {
            return response()->json([
				'success' => false
			]);
        }
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);

        try {
            $order = new Order();
            $order->cart = serialize($cart);
            foreach ($cart->items as $cartproduct) {
                $product = Product::find($cartproduct['item']['id']);
                $product->qty -= $cartproduct['qty'];
                if ($product->durum == 0) {
                    $errorstr = $product->title . " adlı ürün stokta kalmadı.";
					return response()->json(['errorstr' => $errorstr]);
                } else {
                    if ($product->qty < 0) {
                        $errorstr = $product->title . " adlı ürün istediğiniz adette bulunmamakta.";
                        return response()->json(['errorstr' => $errorstr]);
                    } else if ($product->qty == 0) {
                        $product->durum = 0;
                        $product->save();
                    } else {
                        $product->save();
                    }
                }
            }
            $order->address = $request->input('address');
            $order->name = $request->input('name');
			$user = User::find($userid);
            $user->orders()->save($order);
        } catch (\Exception $e) {
            return response()->json([
				'error' => $e
			]);
        }
        Session::forget('cart');
        return response()->json([
			'success' => true
		]);
    }
}
