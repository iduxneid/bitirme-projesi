<?php

namespace App\Http\Controllers;

use App\Comment;
use App\User;
use App\Product;
use App\Follow;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests;
use Auth;
use Session;
use Carbon\Carbon;

class RNUserController extends Controller
{
    public function postSignup(Request $request)
    {
        $this->validate($request, [
            'email' => 'email|required|unique:users',
            'password' => 'required|min:4'
        ]);
        $user = new User([
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password'))
        ]);
        $user->save();
		return response()->json($user);
    }
	
	public function postSignin(Request $request)
    {
        $this->validate($request, [
            'email' => 'email|required',
            'password' => 'required|min:4'
        ]);
		$user = User::where('email', '=', $request->input('email'))->first();
		if ($user !== null){
			if (Hash::check($request->input('password'), $user->password)) {
				return response()->json($user);
			}
			else {
				return response()->json([
					'success' => false
				]);
			}
		}
		else {
			return response()->json([
				'success' => false
			]);
		}
    }

    public function postChangeEmail(Request $request, $userid){
        $this->validate($request, [
            'email' => 'email|required'
        ]);
        $user = User::find($userid);
        $user->email = $request->input('email');
        $user->save();
        return response()->json($user);
    }

    public function postChangePass(Request $request, $userid){
        $this->validate($request, [
            'password' => 'required|min:4'
        ]);
        $user = User::find($userid);
        $user->password=bcrypt($request->input('password'));
        $user->save();
        return response()->json($user);
    }

    public function notify($id, $userid)
    {
        $user_id = $userid;
        $products = Product::all();
        $follows = Follow::where('user_id', $user_id)->limit(6)->get();
        $i_urunler = array();
        $b_urunler = array();
        foreach ($products as $product) {
            $f_price = (($product->price - $product->minprice) / 2) + $product->minprice;

            if ($product->current_price < $f_price) {
                $i_urunler[] = $product;
            }
        }

        foreach ($follows as $follow) {
            foreach ($i_urunler as $i_urun) {
                if($follow->seen < 1 && $id=="1"){
                if ($follow->product_id == $i_urun->id) {
                    $b_urunler[] = $i_urun;
                    $follow->seen = 1;
                    $follow->save();
                }}
                else if($follow->seen < 1 && $id=="0"){
                    if ($follow->product_id == $i_urun->id) {
                        $b_urunler[] = $i_urun;
                        $follow->save();
                }}
            }
        }
        $json_list = json_encode($b_urunler);
        return response()->json($json_list);
    }

    public function getProfile($userid)
    {
        $user = User::where('id', $userid)->first();
        $orders = $user->orders;
        $orders->transform(function ($order, $key) {
            $order->cart = unserialize($order->cart);
            return $order;
        });

        $user_id = $user->id;
        $products = Product::all();
        $follows = Follow::where('user_id', $user_id)->get();
        $i_urunler = array();
        $b_urunler = array();
        foreach ($products as $product) {
            $i_urunler[] = $product;
        }
        foreach ($follows as $follow) {
            foreach ($i_urunler as $i_urun) {
                if ($follow->product_id == $i_urun->id) {
                    $b_urunler[] = $i_urun;
                }
            }
        }

        return response()->json([
            'orders' => $orders,
            'b_urunler' => $b_urunler
        ]);
    }

    public function getNotification($userid)
    {
        $user_id = $userid;
        $products = Product::all();
        $follows = Follow::where('user_id', $user_id)->get();
        $i_urunler = array();
        $b_urunler = array();
        foreach ($products as $product) {
            $f_price = (($product->price - $product->minprice) / 2) + $product->minprice;

            if ($product->current_price < $f_price) {
                $i_urunler[] = $product;
            }
        }

        foreach ($follows as $follow) {
            foreach ($i_urunler as $i_urun) {
                if ($follow->product_id == $i_urun->id) {
                    $b_urunler[] = $i_urun;
                }
            }
        }
        
        return response()->json([
            'b_urunler' => $b_urunler
        ]);
    }
}
